/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fireworks;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.util.ArrayDeque;
import java.util.Queue;
import javax.swing.JPanel;

/**
 *
 * @author Johan
 */
public class Particle {


    /**
     * Class to store a position of the particle.
     */    
    class Point {
        /**
         * Position of the particle.
         */
        public float x,y;

        /**
         * Constructor that can be used to intialize the object. 
         * @param x
         * @param y 
         */
        private Point(float x, float y) {
            this.x = x;
            this.y = y;
        }
    }
    
    /**
     * Queue that is used to give the particle a trail. 
     */
    Queue<Point> trail;
    
    /**
     * Position of the particle.
     */
    private float x,y;      // Position
    /**
     * Speed of the particle.
     */
    private float vx, vy;   // Speed
    /**
     * Acceleration of the particle.
     */
    private float ax,ay;    // Accelaration
    
    /**
     * Delta increment for the simulation of the particle.
     */
    private final float deltaT = 1f/30f;    // Time interval.
    
    /**
     * Red color component of the particle.
     */
    private float red;
    /**
     * Green color component of the particle.
     */
    private float green;
    /**
     * Blue color component of the particle.
     */
    private float blue;

    /**
     * Live time of the particle can be used to check if the particle is still alive. 
     */
    private float liveTime = 0;

    /**
     * Reference to the panel on which the particle is drawn. 
     */
    private final JPanel parent;
    
    /**
     * Constructor. 
     * @param panel Reference to panel to draw on.
     * @param x start x position
     * @param y start y position.
     */
    Particle(JPanel panel, float x, float y) {
        this.parent = panel;
        this.x = x;
        this.y = y;
        
        trail = new ArrayDeque<Point>();
    }
    
    
    /**
     * Set the color as a RGB value of a pixel.
     * @param red
     * @param green
     * @param blue 
     */
    void setColor(float red, float green, float blue) {
        this.red = red;
        this.green = green;
        this.blue = blue;        
    }
    
    /**
     * This sets the time a particle can live. 
     * It is the responsibility from the caller to clean up.
     * @param time 
     */
    void setLiveTime(float time) {
        this.liveTime = time;
    }
    

    /**
     * Get current x-position of the particle.
     * @return 
     */
    public float getY() {
        return y;
    }
    
    /**
     * Get current y-position of the particle.
     * @return 
     */
    public float getX() {
        return x;
    }
        
    /**
     * Can be used to detect that the vertical speed has reached zero.
     * @return 
     */
    public boolean isSpeedZero() {
        return vy < 0.1;
    }
    
    /**
     * This function can be called to determine that the particles
     * live time has passed. 
     * @return True when the particle is dead. 
     */
    public boolean isDead () {
        return liveTime < 0;            
    }
    
    /**
     * Initialize the particle call after the panel is valid.
     */
    public void init() {
        // Start speed.
        vy = (float)Math.random() * 250 + 100;
        vx = (float)Math.random() * 200f - 100f;
        
        // Only gravitational pull for now. 
        ax = 0f;
        ay = -100f;
    }
    
    /**
     * This takes a vector with a given magnitude and an angle and translates it
     * into velocity vector.
     * @param mag Magnitude of the speed.
     * @param angle Angle in degrees.
     */
    public void setSpeed(float mag, float angle) {        
        vx = mag * (float) Math.cos(angle*Math.PI/180.0);
        vy = mag * (float) Math.sin(angle*Math.PI/180.0);      
    }
    
    /**
     * This updates the position of the particle. 
     */
    public void gameUpdate(){
        // Solve integral equations. 
        vx = vx + ax * deltaT;
        x = x + vx * deltaT;
        
        vy = vy + ay * deltaT;
        y = y + vy * deltaT;

        // Add point to trail queue.
        Point p = new Point(x,y);
        trail.add(p);
        
        // Add 20 trailing points after that stop.
        if (trail.size() > 20)
            trail.remove();
        
        // Decrement live time of the particle.
        liveTime--;
    }
    
    /**
     * Render the particle on to the screen. 
     * @param g 
     */
    public void gameRender(Graphics2D g)
    {
        // Calc the trail an percentage of the color
        // this is not correct in the sRGB space but ends up with
        // a very nice approximation.
        float percent = 0.1f;
        float percentStep = 0;
        
        float colorBegin = 1.0f;
        float alpha = 0.8f;
        
        // Calculate the percentStep on basis of the trail length. 
        if (trail.size() > 0)
        {
            percentStep = (1 - percent) / trail.size();        
        }
            
        // Paint the trail this also contains the current particle position. 
        for (Point p : trail) {
            ((Graphics)g).setColor (new Color (red*percent,green*percent,blue*percent, alpha));
            ((Graphics)g).fillOval(Math.round(p.x), parent.getHeight()-Math.round(p.y), 10,10);
            
            percent += percentStep;            
        }
    }    
}
