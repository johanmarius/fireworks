/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fireworks;

import java.awt.Graphics2D;
import java.io.File;
import java.util.Iterator;
import java.util.Vector;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.JPanel;

/**
 * This emulates the sky rocket.
 * It is build up out of particles (Particle.java)
 * @author Johan
 */
public class SkyRocket {

    /**
     * State variable, it either flies or it explodes.
     */
    enum e_fireState {FLY, EXPLOSION };
    /**
     * State variable.
     */
    e_fireState fireState;
    
    /**
     * Start position in the horizontal plane.
     */
    private int xBegin;
    
    /**
     * Vector that contains all the particles currently being rendered 
     * for the sky rocket.
     */
    private Vector<Particle> particles;
    
    /**
     * Reference to the panel on which the graphics are being rendered. 
     */
    private JPanel parent;
    
    
    SkyRocket(JPanel panel,int xBegin) {
        this.parent = panel;
        this.xBegin = xBegin;
        particles = new Vector<Particle>();
        fireState = e_fireState.FLY;
    }
    
    /**
     * Initialize the particle call after the width and height is set.
     */
    public void init() {
        particles.add(new Particle(parent, xBegin, 0));
        
        for (Particle p : particles) {
            p.init();            
            p.setColor(1f,1f,1f);
        }        
    }
    
    /**
     * This updates the mechanics/mathematical model  of the sky rocket.
     */
    public void gameUpdate(){

        if (fireState == e_fireState.FLY) {
            float x = 0, y = 0;
            boolean explosion =  false;
            
            // Use an iterator to be able to remove particles from the
            // vector. 
            Iterator<Particle> iter = particles.iterator();
            
            while(iter.hasNext()) {
                Particle p;
                p = iter.next();
                p.gameUpdate();
                // When speed reaches zero of the particle remove it. 
                if (p.isSpeedZero()) 
                {
                    x = p.getX();
                    y = p.getY();
                    
                    iter.remove();     
                    // Go to explosion and play a sound. 
                    explosion = true;
                    playSound("Explosion+1.wav");
                }                    
            }            
            
            // Start the explosion. 
            if (explosion)
            {   
                // Add 20 particles for the explosion. 
                for (int i=0;i < 20;i++) {
                    particles.add(new Particle(parent, x, y));
                }                
                
                // Calculate the directions of the particles in the explosion.
                float angle = 0;
                float magnitude = 200;
                // Set a random color for the explosion.
                float red = (float)(Math.random());
                float green = (float)(Math.random());
                float blue = (float)(Math.random());
                
                // Apply the above propeties to all the particles.                    
                for (Particle p : particles) {
                    p.init();
                    p.setSpeed(magnitude, angle);                    
                    p.setLiveTime(30);                    
                    // Choose between to two. 
                    // 1 Each direction has a different color
                    // 2 The explosion has the same color.
                    //p.setColor((float)(Math.random()), (float)(Math.random()),(float)(Math.random()));
                    p.setColor((float)red, (float)green,(float)blue);
                      
                    // Calculate the next angle
                    angle += 360.0 / (float)particles.size();
                }
                
                // Got to the explosion state. 
                fireState = e_fireState.EXPLOSION;
            }
        }else
        {
            // Handle the explosion using an iteration.
            // This makes it possible to remove the particles.
            Iterator<Particle> iter = particles.iterator();
            
            // Check and update all the particles mechanics.  
            while(iter.hasNext()) {
                Particle p;
                p = iter.next();
                if (p.isDead())
                {
                    iter.remove();
                }else
                {
                    p.gameUpdate();                    
                }                
            }
            
            // If all particles have died in the explosion.
            // reset this sky rocket to lauch again from another position. 
            if (particles.size() == 0)
            {
                particles.add(new Particle(parent, (int)(Math.random() * parent.getWidth()) , 0));

                for (Particle p : particles) {
                    p.init();  
                    p.setColor(1.0f,1.0f,1.0f);
                }        
                fireState = e_fireState.FLY;
            }            
        }  
    }
    
    /**
     * This renders the graphics to the screen.
     * @param g 
     */
    public void gameRender(Graphics2D g)
    {
        for (Particle p : particles) {
            p.gameRender(g);            
        }
    }
    
    /**
     * This function plays an explosion sound when the sky-rocket reaches
     * it zero velocity. 
     * @param url 
     */
    public static synchronized void playSound(final String url) {
        new Thread(new Runnable() {
            // The wrapper thread is unnecessary, unless it blocks on the
            // Clip finishing; see comments.
            public void run() {
                try {
                    Clip clip = AudioSystem.getClip();
                    AudioInputStream inputStream = AudioSystem.getAudioInputStream(new File(url));
                    clip.open(inputStream);
                    clip.start();
                } catch (Exception e) {
                    System.out.println("System error");
                    System.err.println(e.getMessage());
                }
            }
        }).start();
    }
}
