/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fireworks;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;

/**
 * This is a simple example of how to implement fireworks.
 * @author Johan
 */
public class GraphicsField  extends JPanel implements Runnable, KeyListener, ComponentListener {
    
    private Thread thread;
    private boolean running;
    private BufferedImage image;
    private final int FPS = 30;
    private Graphics2D g;
    private double averageFPS;
    
    public final int HEIGHT_PLAYFIELD = 700;
    public final int WIDTH_PLAYFIELD = 1200;

    
    Vector<SkyRocket> fireWorks;


    
    GraphicsField() {
        super();
        setPreferredSize(new Dimension(WIDTH_PLAYFIELD, HEIGHT_PLAYFIELD));
        setFocusable(true);
        requestFocus();
        
        fireWorks = new Vector<>();
        addComponentListener(this);
    }

    
    /**
     * This method is called on construction. 
     * It will start the thread and install a keylistener. 
     */
    @Override
    public void addNotify() {
        super.addNotify(); //To change body of generated methods, choose Tools | Templates.
    
                // Uncomment this to listen to key presses. 
        addKeyListener(this);
        
        
    }
    
    /**
     * This is the main loop that draws the graphics with 30 FPS. 
     */
    @Override
    public void run() {
        
        // Run in an loop until requested otherwise.
        running = true;
                
        // Create the back buffer.
        image = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);
        g = (Graphics2D) image.getGraphics();
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        
        // Administration for the rendering.        
        long startTime;
        long URDTimeMillis;
        long waitTime;
        long totalTime = 0;
        
        int frameCount = 0;
        int maxFrameCount = 30;
        
        // 
        long targetTime = 1000 / FPS;
        
        
        // Add the sky rockets to the canvas. 
        for (int i=0;i < 10;i++)
        {
            fireWorks.add(new SkyRocket(this, (int)(Math.random()*getWidth())));            
        }
        
        for (SkyRocket p : fireWorks) {
            p.init();
        };
        
        // GAME LOOP
        while (running) {         
            
            startTime = System.nanoTime();
            
            gameUpdate();
            gameRender();
            gameDraw();
            
            URDTimeMillis = (System.nanoTime() - startTime) / 1000000;
        
            waitTime = targetTime - URDTimeMillis;
            
            try {
                Thread.sleep(waitTime);
            } catch (Exception e) {
            }
        
            totalTime += System.nanoTime() - startTime;
            frameCount++;
            if (frameCount == maxFrameCount){
                averageFPS = 1000.0 / ((totalTime/frameCount)/1000000);
                frameCount = 0;
                totalTime = 0;
            }
        }
        fireWorks.clear();
    }
    
    /**
     * Update all mechanics. 
     */
    private void gameUpdate() {
        // Do all the updating here including calculations
        for (SkyRocket p : fireWorks) {
            p.gameUpdate();            
        }
    }

    /**
     * This routine will render/draw the actual back drop, sky rockets, 
     * and its particles. The rendering is done to a back buffer to
     * suppress flickering. 
     */
    private void gameRender() {
        // Draw background
        g.setColor(new Color(0, 0, 0));
        //g.fillRect(0, 0, WIDTH_PLAYFIELD, HEIGHT_PLAYFIELD);
        g.fillRect(0, 0, getWidth(), getHeight());
        
        g.setColor(new Color(1f,1f,1f));
        FontMetrics metrics = g.getFontMetrics();
        
        int fontHeight = metrics.getHeight();
        String s = "Een heel gezond en gelukkig 2018 gewenst";
        int fontWidth = metrics.stringWidth(s);
        
        g.setFont(new Font("Century Gothic", Font.PLAIN, 36));
        g.drawString(s, getWidth()/2 - (fontWidth+2)/2, getHeight()/2 - (fontHeight+2));        
            
        s = "Feliz ano nove e a bom 2018";
        fontWidth = metrics.stringWidth(s);
        
        g.setFont(new Font("Century Gothic", Font.PLAIN, 36));
        g.drawString(s, getWidth()/2 - (fontWidth+2)/2, getHeight()/2 + (fontHeight+2)/2);        
        
        
        for (SkyRocket p : fireWorks) {
            p.gameRender(g);            
        }
    }

    /**
     * Copy everything from the back buffer to the front buffer. 
     */
    private void gameDraw() {
        // Draw everything on the screen double 
        Graphics g2 = this.getGraphics();        
        g2.drawImage(image, 0, 0, null);
        g2.dispose();
    }

    /**
     * Handle the Q key for exiting the simulation.
     * @param ke 
     */
    @Override
    public void keyTyped(KeyEvent ke) {
        if (ke.getKeyChar() == 'q'){
            running = false;
            try {
                thread.join();
            } catch (InterruptedException ex) {
                Logger.getLogger(GraphicsField.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.exit(0);
        }
    }

    /**
     * Not used.
     * @param ke 
     */
    @Override
    public void keyPressed(KeyEvent ke) {
    }

    /**
     * Not used. 
     * @param ke 
     */
    @Override
    public void keyReleased(KeyEvent ke) {
    }

    /**
     * On a resize start the thread, if it all ready running end it and start
     * it again. 
     * @param e 
     */
    @Override
    public void componentResized(ComponentEvent e) {
        if (thread == null) {
            thread = new Thread(this);
            thread.start();
        }else {
            running = false;
            try {
                thread.join();
            } catch (InterruptedException ex) {
                Logger.getLogger(GraphicsField.class.getName()).log(Level.SEVERE, null, ex);
            }
            running = true;
            thread = new Thread(this);
            thread.start();
        }             
    }

    /**
     * Not used. 
     * @param e 
     */
    @Override
    public void componentMoved(ComponentEvent e) {
    }
    
    /**
     * Not used. 
     * @param e 
     */
    @Override
    public void componentShown(ComponentEvent e) {
       
    }

    /**
     * Not used. 
     * @param e 
     */
    @Override
    public void componentHidden(ComponentEvent e) {
    }
}
